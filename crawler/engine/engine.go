package engine

import (
	"../fetcher"
	"../zhenai"
	"fmt"
	"log"
	"strconv"
)


func Run() {
	city := zhenai.GetCityList()
	result := ParseResult{}
	items := Items{}
	for _,v := range city {

		//处理城市分页
		var  url string
		for i:=1; i<10; i++ {
			url = string(v[1]) + "/nv/" + strconv.Itoa(i)		//匹配女的
			//url = string(v[1]) + "/" + strconv.Itoa(i)
			data,err := fetcher.GoFetch(url)

			if err!=nil {
				break
			} else {
				items.Url = url
				items.City = string(v[2])
				PersonInfo := zhenai.GetPersonInfo(data)
				log.Printf("Fetching %s", url)
				log.Printf("PersonInfo %s", PersonInfo)
				//fmt.Println(PersonInfo)
				result.MemberList = append(result.MemberList,PersonInfo)
			}
		}
	}
	fmt.Println(result)
}



//func Run() {
//	city := zhenai.GetCityList()
//
//	//result := ParseResult{}
//	for _,v := range city {
//		//处理城市分页
//		var  url string
//		for i:=1; i<10; i++ {
//			url = string(v[1]) + "/" + strconv.Itoa(i)
//			data,err := fetcher.GoFetch(url)
//
//			if err!=nil {
//				break
//			} else {
//				PersonInfo := zhenai.GetPersonInfo(data)
//				fmt.Println(PersonInfo)
//			}
//		}
//	}
//	//fmt.Println(result)
//}