package fetcher

import (
	"errors"
	"io/ioutil"
	"net/http"
	"regexp"
)

func FetchUrl(url string, rex string) [][][]byte {
	resp,err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		panic(errors.New(url + "访问错误" + string(resp.StatusCode)))
	}
	body, errs := ioutil.ReadAll(resp.Body)
	if errs != nil {
		panic(errs)
	}
	res := regexp.MustCompile(rex)
	match := res.FindAllSubmatch(body,-1)
	return match
}

func GoFetch(url string) ([]byte, error) {
	resp,err := http.Get(url)
	if err != nil {
		return  nil ,err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return  nil, errors.New(url + "访问错误" + string(resp.StatusCode))
	}
	body, errs := ioutil.ReadAll(resp.Body)
	return body,errs
}


func MatchData(body []byte, rex string) [][]byte {
	res := regexp.MustCompile(rex)
	match := res.FindAll(body,-1)
	return match
}