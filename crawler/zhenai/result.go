package zhenai

import (
	"../../crawler/fetcher"
	"encoding/json"
	"strings"
	"time"
)

const url  =  "https://www.zhenai.com/zhenghun"


//获取单个城市的用户列表
func GetPersonInfo(data []byte) []MemberList {
	//防止封锁ip
	time.Sleep(3 * time.Second)

	//计算初始位置
	str := strings.Index(string(data),"window.__INITIAL_STATE__=")
	str2 := strings.Index(string(data[str:]),"{")
	//计算末尾位置
	strEnd := strings.Index(string(data),";(function(){")
	bodyContent := data[str+str2:strEnd]
	//fmt.Println(bodyContent)

	var jsonArr PersonList
	json.Unmarshal(bodyContent,&jsonArr)
	personList := jsonArr.MemberListData.MemberList

	return personList

	//for i:=0; i < len(personList); i++{
	//	if personList[i].Sex == "1" {
	//		personList[i].Sex = "女"
	//	} else {
	//		personList[i].Sex = "男"
	//	}
	//	fmt.Println(personList[i])
	//}
}


//func GetPersonInfo2() []MemberList {
//	//防止封锁ip
//	time.Sleep(3 * time.Second)
//
//	var url = "http://www.zhenai.com/zhenghun/wuxi"
//	//var rex  =  `\[{.+}\]`
//	data,err := fetcher.GoFetch(url)
//	if err!=nil {
//		panic(err)
//	}
//	//fmt.Println(string(data))
//	//计算初始位置
//	str := strings.Index(string(data),"window.__INITIAL_STATE__=")
//	str2 := strings.Index(string(data[str:]),"{")
//	//计算末尾位置
//	strEnd := strings.Index(string(data),";(function(){")
//	bodyContent := data[str+str2:strEnd]
//	//fmt.Println(bodyContent)
//
//	var jsonArr PersonList
//	json.Unmarshal(bodyContent,&jsonArr)
//	personList := jsonArr.MemberListData.MemberList
//
//	return personList
//
//	//for i:=0; i < len(personList); i++{
//	//	if personList[i].Sex == "1" {
//	//		personList[i].Sex = "女"
//	//	} else {
//	//		personList[i].Sex = "男"
//	//	}
//	//	fmt.Println(personList[i])
//	//}
//}


//获取城市列表
func GetCityList() [][][]byte {
	var rex  =  `<a href="(http://www.zhenai.com/zhenghun/\w+)" [^>]*>([^<]+)</a>`
	data := fetcher.FetchUrl(url,rex)
	return  data
}




