package zhenai

type Items struct {
	City       []interface{}
	MemberList []MemberList
}


type PersonList struct {
	MemberListData       MemberListData 	`json:"memberListData"`
	RecommendListData 				string `json:"recommend_list_data"`
	FooterData 						string `json:"footer_data"`
	NavigationData					string `json:"navigation_data"`
	RecommendRegisterListData		string `json:"recommend_register_list_data"`
}

type MemberListData struct {
	CurrentPage int8  		  "currentPage"
	MemberList  []MemberList  `json:"MemberList"`
	pageInfos   string 		  `json:"pageInfos"`
	TkdInfo		string 		  `json:"tkd_info"`
	Total       int8  		  "total"
}



type MemberList struct {
	MemberID				int32   `memberID`
	AvatarURL				string  `avatarURL`
	NickName				string  `nickName`
	Age						int  	`age`
	Height					string  `height`
	Marriage				string  `marriage`
	Sex						string  	`sex`
	WorkCity				string  `workCity`
	Salary					string  `salary`
	Occupation				string  `occupation`
	Car						string  `car`
	Children				string  `children`
	Constellation			string  `constellation`
	Education				string  `education`
	House					string  `house`
	LastModTime				string  `lastModTime`
	IntroduceContent		string  `introduceContent`
}
