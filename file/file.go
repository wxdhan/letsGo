package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func goErr(err error,parm string){
	if err != nil {
		fmt.Println(parm+":",err)
		return
	}
}

func main(){
	//文件操作
	file,err := os.OpenFile("D:/GO/letsGo/file/text/a.text",os.O_CREATE|os.O_WRONLY,os.ModePerm)
	//file,err := os.Open("D:/GO/letsGo/file/text/a.text")
	goErr(err,"Open")
	defer file.Close()

	//a := make([]byte,4,4)
	//a := []byte{97,98,99,100}
	//read,errs := file.Read(a)
	//if errs!=nil {
	//	fmt.Println("read的err:",errs)
	//	return
	//}
	//fmt.Println(a)
	//fmt.Println(string(a))
	//fmt.Println(read)
	//
	//w,e := file.Write(a)
	//if e != nil {
	//	fmt.Println("write:",e)
	//	return
	//}
	////goErr(e,"write")
	//fmt.Println(a)
	//fmt.Println(w)
	//
	//ws,es := file.WriteString("Hello word!")
	//if es != nil {
	//	fmt.Println("write:",es)
	//	return
	//}
	//fmt.Println(ws)

	fileTill,ioutilErr:=ioutil.ReadFile("D:/GO/letsGo/file/text/a.text")
	if ioutilErr != nil {
		fmt.Println("ioutil:",ioutilErr)
		return
	}
	fmt.Println(string(fileTill))


	//file,err := os.Stat("D:/GO/letsGo/file/a.text")
	//if err!=nil {
	//	fmt.Println("err:",err)
	//	return
	//}
	//fmt.Printf("%T\n",file)
	//fmt.Println(file.ModTime())
	//fmt.Println(filepath.Abs("D:/GO/letsGo/file/a.text"))

	//创建文件夹
	//errs := os.Mkdir("D:/GO/letsGo/file/b.text",0777)
	//if errs != nil {
	//	fmt.Println("err:",errs)
	//}

	//创建文件
	//create,e := os.Create("D:/GO/letsGo/file/text/a.text")
	//if e != nil {
	//	fmt.Println("err:",e)
	//	return
	//}
	//fmt.Println(create.Name())

	//删除文件
	//e := os.RemoveAll("D:/GO/letsGo/file/a.text")
	//fmt.Println(e)


	//重命名文件
	//os.Rename("D:/GO/letsGo/file/b.text","D:/GO/letsGo/file/text")
}
