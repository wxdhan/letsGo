package main

import (
	"fmt"
	"io"
	"os"
	"strings"
	"time"
)

func toErr(err error,parm string){
	if err != nil {
		fmt.Println(parm+":",err)
		return
	}
}

//复制文件
func main() {
	fmt.Println(time.Now())
	image := "D:/GO/letsGo/file/text/timg.jpg"
	str := "D:/GO/letsGo/file/text/"+image[strings.LastIndex(image,"/")+1:]
	//strTemp := str+"temp.text"
	strTemp := "D:/GO/letsGo/file/text/timg2.jpg"
	file1,_ := os.Open(image)
	file2,_ := os.OpenFile(str, os.O_CREATE|os.O_RDWR, os.ModePerm)
	file3,_ := os.OpenFile(strTemp, os.O_CREATE|os.O_RDWR, os.ModePerm)
	//toErr(err1,"20")
	//toErr(err2,"21")
	//toErr(err3,"22")
	//fmt.Println(file1)
	//fmt.Println(file2.Name())
	//fmt.Println(file3)

	defer file1.Close()
	defer file2.Close()
	defer file3.Close()

	splice := make([]byte,1024,1024)
	total := 0
	for{
		data,err := file1.Read(splice)
		toErr(err,"28")
		if err == io.EOF || data == 0 {
			fmt.Println("文件读取完毕")
			break
		}
		total += data
		file3.Write(splice)
	}
fmt.Println(total)//249965

	fmt.Println(time.Now())
}
