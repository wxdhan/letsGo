package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
)

func main()  {
	var url = "http://www.zhenai.com/zhenghun/wuxi"
	resp,err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		panic(errors.New(url + "访问错误" + string(resp.StatusCode)))
	}
	body, _ := ioutil.ReadAll(resp.Body)
	//fmt.Println(string(body))
	file,err:=os.OpenFile("www.zhenai.com.html",os.O_RDWR|os.O_CREATE,os.ModePerm)
	if err!=nil {
		fmt.Println(err)
	}
	file.Write(body)
}
